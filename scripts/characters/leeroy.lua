
return {
  name = "Leeroy Jenkins",
  ability_scores = {
    15,
    14,
    13,
    12,
    10,
    8
  },
  race = "dwarf",
  levels = {
    "fighter"
  }
}

