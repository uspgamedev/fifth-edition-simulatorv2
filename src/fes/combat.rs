
use fes::character::Character;
use std::boxed::Box;

pub struct Combat<'a> {
    combatants: Vec<Box<Character<'a>>>,
}

pub fn new<'a>() -> Combat<'a> {
    Combat { combatants: Vec::new() }
}

impl<'a> Combat<'a> {
    pub fn add_combatant(&mut self, combatant: Box<Character<'a>>) {
        self.combatants.push(combatant);
    }
    pub fn play_a_round(&mut self) {
        for combatant in self.combatants.iter_mut() {
            combatant.take_damage(2);
        }
        for combatant in self.combatants.iter() {
            combatant.print(false);
        }
    }
}
