
#![allow(dead_code)]

extern crate rand;

#[derive(Clone)]
pub struct Dice {
    sides: u8,
}

impl Dice {
    pub fn new(sides: u8) -> Dice {
        Dice { sides: sides }
    }
    pub fn sides(&self) -> u8 {
        self.sides
    }
    pub fn roll(&self) -> u8 {
        1 + rand::random::<u8>() % self.sides
    }
}

pub const D6: Dice = Dice { sides: 6 };
pub const D8: Dice = Dice { sides: 8 };
pub const D10: Dice = Dice { sides: 10 };
pub const D12: Dice = Dice { sides: 12 };
pub const D20: Dice = Dice { sides: 20 };
pub const D100: Dice = Dice { sides: 100 };
