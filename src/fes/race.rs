
extern crate lua;
use fes::effect::EffectKeeper;
use fes::character::AbilityScoreType;
use luawrapper;

struct RacialTrait {
    name: String,
    fx_keeper: EffectKeeper,
}

pub struct Race {
    full_name: String,
    traits: Vec<RacialTrait>,
}

impl Race {
    pub fn total_ability_score_modifier(&self, which: &AbilityScoreType) -> i8 {
        self.traits.iter().fold(0,
                                |acc, x| acc + x.fx_keeper.total_ability_score_modifier(which))
    }
}

fn new(full_name: String, traits: Vec<RacialTrait>) -> Race {
    Race {
        full_name: full_name,
        traits: traits,
    }
}

pub fn from_script(filename: &'static str) -> Race {
    let mut wrapper = luawrapper::wrapper::new(lua::State::new());
    wrapper.load_file(filename);
    wrapper.step_into_call_result();
    let name = wrapper.get_field::<&str, String>("full_name");
    wrapper.step_into_field("traits");
    let mut traits = Vec::<RacialTrait>::new();
    let len = wrapper.get_len();
    for i in 1..len + 1 {
        wrapper.step_into_field(i);
        let name = wrapper.get_field::<&str, String>("name");
        wrapper.step_into_field("effects");
        let fx_len = wrapper.get_len();
        let mut fx_keeper = EffectKeeper::new();
        for j in 1..fx_len + 1 {
            wrapper.step_into_field(j);
            let effect_type = wrapper.get_field::<&str, String>("type");
            if effect_type == "ability_score_change" {
                let which = wrapper.get_field::<&str, String>("which");
                let value = wrapper.get_field::<&str, i64>("value") as i8;
                fx_keeper.add_ability_score_modifier(
                    &AbilityScoreType::from_string(which.as_str()),
                    value,
                );
            }
            wrapper.step_back();
        }
        let racial_trait = RacialTrait { name, fx_keeper };
        traits.push(racial_trait);
        wrapper.step_back();
        wrapper.step_back();
    }
    wrapper.step_back();
    return new(name, traits);
}
