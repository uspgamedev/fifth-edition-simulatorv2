extern crate lua;

pub struct Wrapper {
    state: lua::State,
    index: i32,
}

pub fn new(state: lua::State) -> Wrapper {
    Wrapper {
        state: state,
        index: 0,
    }
}

impl Wrapper {
    pub fn load_file(&mut self, filename: &'static str) {
        if self.state.load_file(filename) != lua::ThreadStatus::Ok {
            println!("Failed to load script");
            panic!()
        }
    }

    pub fn step_into_call_result(&mut self) {
        self.state.call(0, 1);
        self.index += 1;
    }

    pub fn step_into_field<T: lua::ToLua>(&mut self, key: T) {
        self.state.push(key);
        self.state.get_table(self.index);
        self.index += 1;
    }

    pub fn step_back(&mut self) {
        self.state.pop(1);
        self.index -= 1;
    }

    pub fn get_field<T: lua::ToLua, R: lua::FromLua>(&mut self, key: T) -> R {
        self.step_into_field(key);
        let top = self.state.get_top();
        let field = R::from_lua(&mut self.state, self.index).unwrap();
        // For Strings, it'll use to_str, which will place the value at the top of the stack.
        // For Integers, it'll use to_integer, which won't place the value at the top of the stack.
        self.state.set_top(top);
        self.step_back();
        field
    }

    pub fn get_vec<T: lua::ToLua, R: lua::FromLua>(&mut self, key: T) -> Vec<R> {
        self.step_into_field(key);
        let mut vec: Vec<R> = Vec::new();
        let size = self.get_len();
        for i in 1..size + 1 {
            vec.push(self.get_field::<i64, R>(i));
        }
        self.step_back();
        vec
    }

    pub fn get_len(&mut self) -> i64 {
        self.state.len(self.index);
        let len = self.state.to_integer(self.index + 1);
        self.state.pop(1);
        len
    }
}
