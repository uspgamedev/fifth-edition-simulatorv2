
extern crate lua;

mod fes;
mod luawrapper;
use fes::database;
use std::boxed::Box;

fn main() {
    let db = database::new();
    let mut combat = fes::combat::new();
    let mut bob = fes::character::from_script(&db, "scripts/characters/bob.lua");
    let mut leeroy = fes::character::from_script(&db, "scripts/characters/leeroy.lua");
    bob.print(true);
    leeroy.print(true);
    combat.add_combatant(Box::new(bob));
    combat.add_combatant(Box::new(leeroy));
    combat.play_a_round();
}
